#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include "menu.h"

using namespace std;

   int main() {
      //create window size and class
      sf::RenderWindow window(sf::VideoMode(800, 600, 32), "Flip");
	//sf::RenderWindow ::clear(sf::Color::White);
      //create sfml texture image method
      sf::Texture backgroundImage;
      sf::RectangleShape play;
      sf::RectangleShape instructions;
      sf::Texture settings;
	 sf::Font font;        
      play.setSize(sf::Vector2f(150, 50));
      play.setFillColor(sf::Color::Red);

      instructions.setSize(sf::Vector2f(150, 50));
      instructions.setFillColor(sf::Color::Red);
      //loads the image from image folder
      if (!backgroundImage.loadFromFile("images/flip-transparent.png"))
         return 1;
	if (!font.loadFromFile("fonts/KGLegoHouse.ttf"))
         return 1;
      //creates object of sprite class
      sf::Sprite Logo;
      sf::Sprite Playbutton;
      //assigns the sprite "texture" to the loaded image
      Logo.setTexture(backgroundImage);
      Logo.setPosition(150,150);
      play.setPosition(200,400);
      instructions.setPosition(500,400);
Menu menu(window.getSize().x, window.getSize().y);

      while (window.isOpen()) {

         if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
      {
         window.close();
      }
			
	    //creates event object
         sf::Event event;
         while (window.pollEvent(event)) {
            //checks to see if user has closed the window
            if (event.type == sf::Event::Closed)
               window.close();
				switch (event.type)
					{
					case sf::Event::KeyReleased:
						switch (event.key.code)
						{
						case sf::Keyboard::Left:
							menu.MoveUp();
							break;

						case sf::Keyboard::Right:
							menu.MoveDown();
							break;

						case sf::Keyboard::Return:
							switch (menu.GetPressedItem())
							{
							case 0:
								std::cout << "The player chose the play button" << std::endl;
								window.clear();
								break;
							case 1:
								std::cout << "The player chose the option button" << std::endl;
								break;
							case 2:
								std::cout << "The player chose the exit button" << std::endl;
								
								break;
							}

							break;
						}

						break;
					case sf::Event::Closed:
						window.close();

						break;

					}

         }   

         //clear the screen and draw the objects
         
window.clear(sf::Color::White);
	 //  window.clear(sf::Color(255, 255,255));
        window.draw(Logo);
         window.draw(play);
         window.draw(instructions);
		menu.draw(window);
         window.display();
      }

      return 0;
   }

