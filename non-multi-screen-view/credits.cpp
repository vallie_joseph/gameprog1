#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>

using namespace std;

   int main() {

      //create window size and class
      sf::RenderWindow window(sf::VideoMode(800, 600, 32), "Flip");
	 sf::Font font;
	 sf::Text musicText;
	 sf::Text soundFxText; 
	 sf::Text signage;
	 if (!font.loadFromFile("fonts/KGLegoHouse.ttf"))
		return 1;
	 soundFxText.setFont(font); 
	 signage.setFont(font); 
	 musicText.setFont(font); 
      
	musicText.setString("Music \n o Techno/ house beat type background music \n http://www.freesound.org/people/Creeper_Ciller78/sounds/346895/ \n o Menu Music/Pause music \n http://www.freesound.org/people/Creeper_Ciller78/sounds/346895/ (softer) \n o Final Battle Music\n  http://incompetech.com/music/royalty-free/index.html?collection=12&Search=Search (Exit the Premises)");
 	
 	soundFxText.setString("Sound FX \n o Tile Flip Sound Effect \n http://www.zapsplat.com/music/cartoon-swipe-grab-or-transition-med-pitched/ \n o Building Level Sound \n http://www.zapsplat.com/music/fantasy-reversed-debris-building-or-structure-re-assemble-by-magic/  ");
	 
 	signage.setString("Created by: Vallie Joseph \n Location: Marist College \n Project Title: C++ Game Project for CMPT 414N 111 16F (Game Programming and Design I) ");
	
 musicText.setPosition(40,50);
	 musicText.setCharacterSize(14);
	 musicText.setFont(font); 
	 musicText.setColor(sf::Color::Black);

	 soundFxText.setPosition(40,255);
	 soundFxText.setCharacterSize(14);
	 soundFxText.setFont(font); 
	 soundFxText.setColor(sf::Color::Black);

 	 signage.setPosition(40,410);
	 signage.setCharacterSize(14);
	 signage.setFont(font); 
	 signage.setColor(sf::Color::Black);
    while (window.isOpen())
    {
if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
      {
         window.close();
      }

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

window.clear(sf::Color::White);
	   window.draw(musicText);
	   window.draw(soundFxText);
	   window.draw(signage);
        window.display();
    }

    return 0;
}

