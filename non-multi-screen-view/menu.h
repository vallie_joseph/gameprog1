/**Notice: All credit for this menu belongs to:
	Sonar Systems Tutorials: https://github.com/SonarSystems
**/
#define menu_options 3
class Menu
{
public:
	Menu(float width, float height);
	~Menu();

	void draw(sf::RenderWindow &window);
	void MoveUp();
	void MoveDown();
	int GetPressedItem() { return selectedItemIndex; }

private:
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[menu_options];

};

Menu::Menu(float width, float height)
{
	if (!font.loadFromFile("fonts/KGLegoHouse.ttf"))
	{
		// handle error
	}

	menu[0].setFont(font);
	menu[0].setColor(sf::Color::White);
	menu[0].setString("Play");
	menu[0].setPosition(250,410);

	menu[1].setFont(font);
	menu[1].setColor(sf::Color::Black);
	menu[1].setString("Options");
	menu[1].setPosition(510,410);

	menu[2].setFont(font);
	menu[2].setColor(sf::Color::Black);
	menu[2].setString("Credits");
	menu[2].setPosition(590,0);

	selectedItemIndex = 0;
}


Menu::~Menu()
{
}

void Menu::draw(sf::RenderWindow &window)
{
	for (int i = 0; i < menu_options; i++)
	{
		window.draw(menu[i]);
	}
}

void Menu::MoveUp()
{
	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setColor(sf::Color::Black);
		selectedItemIndex--;
		menu[selectedItemIndex].setColor(sf::Color::White);
	}
}

void Menu::MoveDown()
{
	if (selectedItemIndex + 1 < menu_options)
	{
		menu[selectedItemIndex].setColor(sf::Color::Black);
		selectedItemIndex++;
		menu[selectedItemIndex].setColor(sf::Color::White);
	}
}


