#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <SFML/Audio.hpp>
#include "TextureHolder.h"
#include "TextureHolder.cpp"
#include "Player.h"
#include "Player.cpp"
#include "Tile.h"
#include "Tile.cpp"
#include "AnimatedSprite.hpp"
#include "AnimatedSprite.cpp"
#include <cstdlib>
#include "Animation.hpp"
#include "Animation.cpp"

// TODO: Add matrix to restrict player to spaces, then move them along the
// matrix and check against win conditions
using namespace sf;
using namespace std;
int main() {
  Vector2i screenDimensions(800, 600);
  // RenderWindow window(VideoMode(800, 600), "Flᴉp");
  RenderWindow window(VideoMode(screenDimensions.x, screenDimensions.y),
                      "Flᴉp");
  bool fullscreen = false;
  TextureHolder holder;
  // Player player;
  Font Font;
  window.setFramerateLimit(60);
  Texture doctorSpriteSheet = TextureHolder::GetTexture("images/scientist.png");
  Texture menuIconTexture =
      TextureHolder::GetTexture("images/flip-transparent.png");
  Texture playTexture = TextureHolder::GetTexture("images/play-button.png");
  Texture optionTexture =
      TextureHolder::GetTexture("images/options-button.png");
  Texture creditTexture =
      TextureHolder::GetTexture("images/credits-button.png");
  Texture quitTexture = TextureHolder::GetTexture("images/quit-button.png");
  Texture goBackTexture = TextureHolder::GetTexture("images/back-button.png");
  Texture soundOnon = TextureHolder::GetTexture("images/on.png");
  Texture soundOffoff = TextureHolder::GetTexture("images/off2.png");
  Texture soundOnoff = TextureHolder::GetTexture("images/on2.png");
  Texture soundOffon = TextureHolder::GetTexture("images/off1.png");
  Texture windowedOn = TextureHolder::GetTexture("images/windowed.png");
  Texture windowedOff = TextureHolder::GetTexture("images/windowed2.png");
  Texture fullscreenOn = TextureHolder::GetTexture("images/fullscreen.png");
  Texture fullscreenOff = TextureHolder::GetTexture("images/fullscreen2.png");
  Texture bgtexture = TextureHolder::GetTexture("images/gamebackground.png");
  Texture cointexture = TextureHolder::GetTexture("images/gold_coin.png");
  Texture nextLevel = TextureHolder::GetTexture("images/nextlvl.png");
  Texture victorytexture =
      TextureHolder::GetTexture("images/Victory-Man-Silhouette.png");

  if (!Font.loadFromFile("fonts/KGLegoHouse.ttf")) {
    cerr << "Error loading KGLegoHouse.ttf" << endl;
    return (-1);
  }



  sf::Clock frameClock;
  int scores, time, currentLevel;
  float speed = 80.f;
  bool noKeyWasPressed = true;

  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) window.close();
      if (event.type == sf::Event::KeyPressed &&
          event.key.code == sf::Keyboard::Escape)
        window.close();
    }

    sf::Time frameTime = frameClock.restart();

    // if a key was pressed set the correct animation and move correctly
    sf::Vector2f movement(0.f, 0.f);

    SoundBuffer walkingBuffer;
    walkingBuffer.loadFromFile("audio/one_hit_from_chinese_blocks.wav");
    Sound walking;
    walking.setBuffer(walkingBuffer);

    Music backgroundMusic;
    if (!backgroundMusic.openFromFile("audio/bgmusic.wav")) {
      cerr << "Error loading music" << endl;
    }
     Music creditMusic;
    if (!creditMusic.openFromFile("audio/dokapi-continental_drift-02-drifting_mind.wav")) {
      cerr << "Error loading music" << endl;
    }
    
    Sprite goldCoin;
    Sprite menuIcon;
    Player player;
    Tile tile;
    Tile tile2;
    Sprite play;
    Sprite continueToNextLevel;
    Sprite victory;
    Sprite backgroundIMG;
    Sprite soundOnToggling;
    Sprite soundOffToggling;
    Sprite windowOnToggling;
    Sprite fullScreenToggling;
    play.setTexture(playTexture);
    play.setPosition(25, 450);
    Sprite credits;
    credits.setPosition(225, 450);
    credits.setTexture(creditTexture);
    Sprite options;
    options.setTexture(optionTexture);
    options.setPosition(425, 450);
    Sprite quit;
    quit.setTexture(quitTexture);
    quit.setPosition(625, 450);
    menuIcon.setTexture(menuIconTexture);
    menuIcon.setPosition(150, 150);
    Text hud;
    hud.setFont(Font);
    hud.setCharacterSize(45);
    hud.setColor(Color::Black);
    Text musicText;
    Text soundFxText;
    Sprite goBack;
    goBack.setPosition(24, 550);
    goBack.setTexture(goBackTexture);
    Text signage;
    soundFxText.setFont(Font);
    continueToNextLevel.setTexture(nextLevel);
    continueToNextLevel.setPosition(25, 450);
    signage.setFont(Font);
    musicText.setFont(Font);
    Clock frameClock;
    float speed = 80.f;
    time=300;
    bool noKeyWasPressed = true;

          scores=0;
          currentLevel=1;
    backgroundMusic.play();
    enum Screens {
      mainmenu,
      optionscreen,
      creditscreen,
      quitscreen,
      levels,
      pause,
      victoryscreen,
      victoryscreen2,
      lossscreen
    };
    Screens screen = mainmenu;

    enum Sounds { soundon, soundoff };
    Sounds sound = soundon;

    while (window.isOpen()) {
      Event event;
      while (window.pollEvent(event)) {
        if (event.type == Event::Closed) window.close();
      }

      int mouseX = Mouse::getPosition(window).x;
      int mouseY = Mouse::getPosition(window).y;
      if (screen == victoryscreen) {
        window.clear(Color::White);
        Text victoryText;
        victoryText.setString("You Won!");
        victoryText.setFont(Font);
        victoryText.setColor(Color::Black);
        victoryText.setPosition(500, 200);
        victory.setTexture(victorytexture);
        victory.setPosition(300, 400);

  if (Mouse::isButtonPressed(Mouse::Left)) {
          if (mouseY >= 450 && mouseY <= 500 && mouseX >= 25 && mouseX <= 175) {
            screen = levels;
          } 
          if (mouseY >= 550 && mouseY <= 650 && mouseX >= 24 && mouseX <= 175) {
            // cout << "Play the game" << endl;
            currentLevel = 2;
          }
        }
        
    goBack.setPosition(24, 550);
    goBack.setTexture(goBackTexture);
        stringstream dd;
        dd << "Final Score:" << scores << "    Total Time Remaining:" << time;
        hud.setString(dd.str());
        window.draw(hud);
        window.draw(victoryText);
        window.draw(victory);
        window.draw(goBack);
        window.draw(continueToNextLevel);
      }
       if (screen == victoryscreen2) {
        window.clear(Color::White);
        Text victoryText;
        victoryText.setString("You Won!");
        victoryText.setFont(Font);
        victoryText.setColor(Color::Black);
        victoryText.setPosition(500, 200);
        victory.setTexture(victorytexture);
        victory.setPosition(300, 400);

  if (Mouse::isButtonPressed(Mouse::Left)) {
          if (mouseY >= 450 && mouseY <= 500 && mouseX >= 25 && mouseX <= 175) {
            screen = levels;
          } 
        }
        
    goBack.setPosition(24, 550);
    goBack.setTexture(goBackTexture);
        stringstream dd;
        dd << "Final Score:" << scores << "    Total Time Remaining:" << time;
        hud.setString(dd.str());
        window.draw(hud);
        window.draw(victoryText);
        window.draw(victory);
        window.draw(goBack);
      }
      
      
      if (screen == lossscreen) {
      
        window.clear(Color::White);
        Text lossText;
          if (Mouse::isButtonPressed(Mouse::Left)) {
               if (mouseY >= 550 && mouseY <= 650 && mouseX >= 24 && mouseX <= 175) {
                 screen = mainmenu;
               } 
          }
          
         time=300;
        lossText.setFont(Font);
        lossText.setColor(Color::Black);
        lossText.setPosition(500, 200);
        stringstream dd;
      dd << "Final Score:" << scores << "Total Time Remaining:" << time;
        lossText.setString("You Lost! Try again?");
        
    goBack.setPosition(24, 550);
    goBack.setTexture(goBackTexture);
        window.draw(goBack);
        hud.setString(dd.str());
        window.draw(lossText);
        window.draw(hud);
		  
      }
      if (screen == levels) {
      
        window.clear(Color::White);
        creditMusic.stop();
        goldCoin.setTexture(cointexture);
        goldCoin.setPosition(200, 500);
        backgroundIMG.setTexture(bgtexture);

      if (Keyboard::isKeyPressed(Keyboard::W)) {
		player.moveUp();  
		walking.play();
		noKeyWasPressed = false;
      } else {
        player.stopUp();
      }

      if (Keyboard::isKeyPressed(Keyboard::S)) {
        player.moveDown();
		walking.play();
		noKeyWasPressed = false;
      } else {
        player.stopDown();
      }

      if (Keyboard::isKeyPressed(Keyboard::A)) {
        player.moveLeft();
		walking.play();
		noKeyWasPressed = false;
      } else {
		walking.play();
        player.stopLeft();
      }

      if (Keyboard::isKeyPressed(Keyboard::D)) {
        player.moveRight();
		walking.play();
		noKeyWasPressed = false;
      } else {
        player.stopRight();
      }
      
      if (Keyboard::isKeyPressed(Keyboard::Escape)) {
          window.close();
      } if (Keyboard::isKeyPressed(Keyboard::Q)) {
         screen=mainmenu;
            backgroundMusic.stop();
      } 
    /*  if(Keyboard::isKeyPressed(Keyboard::Space)){
		//to have tile flip action soon
		flipnoise.play();	
	}*/

      //Update Player
      player.update();
      tile.update();

        std::stringstream ss;
        ss <<"Level: "<< currentLevel << " Score:" << scores << "Time Left:" << time;
        hud.setString(ss.str());
          
        time= time -0.0005;
        if ((player.getPosition()).contains(goldCoin.getPosition())) {
          cout << "player is touching the coin" << endl;
          scores++;
          if (scores == 150) {
            screen = victoryscreen;
          } if (scores == 200) {
            screen = victoryscreen;
          }
        }
        if (time == 0) {
                 screen = lossscreen;
               }
        if((currentLevel==2)){
			if ((player.getPosition()).contains(goldCoin.getPosition())) {
          cout << "player is touching the coin" << endl;
          scores++;
          if (scores == 150) {
            screen = victoryscreen2;
          }
               
		}
	}
	//	tile2.setPosition(100,100);
       window.draw(backgroundIMG);
        window.draw(hud);
	   window.draw(tile.getRectangle());
	   window.draw(tile2.getRectangle());
	   window.draw(player.getSprite());
        window.draw(goldCoin);
      }
      if (screen == mainmenu) {
        window.clear(Color::White);

          scores=0;
        window.draw(menuIcon);
        window.draw(play);
        window.draw(options);
        window.draw(credits);
        window.draw(quit);
        if (Mouse::isButtonPressed(Mouse::Left)) {
          if (mouseY >= 450 && mouseY <= 500 && mouseX >= 25 && mouseX <= 175) {
            screen = levels;
          } else if (mouseY >= 450 && mouseY <= 500 && mouseX >= 425 &&
                     mouseX <= 575) {
            screen = optionscreen;
          } else if (mouseY >= 450 && mouseY <= 500 && mouseX >= 225 &&
                     mouseX <= 375) {
            screen = creditscreen;
          } else if (mouseY >= 450 && mouseY <= 500 && mouseX >= 625 &&
                     mouseX <= 775) {
            screen = quitscreen;
            window.close();
          } else {
            // cout << " " << endl;
          }
        }
      }
      if (screen == optionscreen) {
        window.clear(Color::White);
        Text soundToggle;
        Text fullScreenToggle;
        soundToggle.setString("Music");
        soundToggle.setFont(Font);
        soundToggle.setPosition(0, 20);
        soundToggle.setColor(Color::Black);
        fullScreenToggle.setString("Video Mode");
        fullScreenToggle.setPosition(0, 180);
        fullScreenToggle.setFont(Font);
        fullScreenToggle.setColor(Color::Black);

        if (Mouse::isButtonPressed(Mouse::Left)) {
          if (mouseY >= 550 && mouseY <= 650 && mouseX >= 24 && mouseX <= 175) {
            // cout << "Play the game" << endl;
            screen = mainmenu;
          }
          if (mouseY >= 220 && mouseY <= 420 && mouseX >= 475 &&
              mouseX <= 600) {
            // cout<<"you clicked the fullscreen button"<<endl;
            fullscreen = true;
            window.create(
                VideoMode(1920, 1080), "Flip",
                (fullscreen ? Style::Default : Style::Close | Style::Resize));
          }
          if (mouseY >= 220 && mouseY <= 420 && mouseX >= 125 &&
              mouseX <= 300) {
            // cout<<"you clicked the fullscreen button"<<endl;
            fullscreen = false;
            window.create(
                VideoMode(800, 600), "Flip",
                (fullscreen ? Style::Default : Style::Close | Style::Resize));
          }
        }
        if (sound == soundon) {
          soundOnToggling.setTexture(soundOffoff);
          soundOffToggling.setTexture(soundOnon);
          soundOffToggling.setPosition(275, 20);
          soundOnToggling.setPosition(100, 20);
          window.draw(soundOnToggling);
          window.draw(soundOffToggling);
        } else if (sound == soundoff) {
        }
        if (fullscreen == true) {
          windowOnToggling.setTexture(windowedOff);
          fullScreenToggling.setTexture(fullscreenOn);
          windowOnToggling.setPosition(125, 220);
          fullScreenToggling.setPosition(475, 220);
          window.draw(windowOnToggling);
          window.draw(fullScreenToggling);

        } else if (fullscreen == false) {
          windowOnToggling.setTexture(windowedOn);
          fullScreenToggling.setTexture(fullscreenOff);
          windowOnToggling.setPosition(125, 220);
          fullScreenToggling.setPosition(475, 220);
          window.draw(windowOnToggling);
          window.draw(fullScreenToggling);
        }

        window.draw(goBack);
        window.draw(soundToggle);
        window.draw(fullScreenToggle);
      }
      if (screen == creditscreen) {
        window.clear(Color::White);
        backgroundMusic.stop();
        musicText.setString(
            "Music \n o Techno/ house beat type background music \n "
            "http://www.freesound.org/people/Creeper_Ciller78/sounds/346895/ "
            "\n o Menu Music/Pause music \n "
            "http://www.freesound.org/people/Creeper_Ciller78/sounds/346895/ "
            "(softer) \n o Final Battle Music\n  "
            "http://incompetech.com/music/royalty-free/"
            "index.html?collection=12&Search=Search (Exit the Premises)");

        soundFxText.setString(
            "Sound FX \n o Tile Flip Sound Effect \n "
            "http://www.zapsplat.com/music/"
            "cartoon-swipe-grab-or-transition-med-pitched/ \n o Building Level "
            "Sound \n "
            "http://www.zapsplat.com/music/"
            "fantasy-reversed-debris-building-or-structure-re-assemble-by-"
            "magic/  ");

        signage.setString(
            "Created by: Vallie Joseph \n Location: Marist College \n Project "
            "Title: C++ Game Project for CMPT 414N 111 16F (Game Programming "
            "and Design I) ");
        if (Mouse::isButtonPressed(Mouse::Left)) {
          if (mouseY >= 550 && mouseY <= 650 && mouseX >= 24 && mouseX <= 175) {
            // cout << "Play the game" << endl;
            screen = mainmenu;
          }
        }
        musicText.setPosition(40, 50);
        musicText.setCharacterSize(14);
        musicText.setFont(Font);
        musicText.setColor(Color::Black);

        soundFxText.setPosition(40, 255);
        soundFxText.setCharacterSize(14);
        soundFxText.setFont(Font);
        soundFxText.setColor(Color::Black);

        creditMusic.play();
        signage.setPosition(40, 410);
        signage.setCharacterSize(14);
        signage.setFont(Font);
        signage.setColor(Color::Black);
        window.draw(musicText);
        window.draw(soundFxText);
        window.draw(signage);
        window.draw(goBack);
      }
      if (screen == quitscreen) {
        creditMusic.stop();
        window.clear(Color::White);
      }

      window.display();
    }
  }
  return 0;
}
