#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class Tile
{
private:
     //Where the tile is
	Vector2f m_Position;
	//Instead of sprite, using Rectangle
     RectangleShape m_Rectangle;
     
     //setting the sizeo f default tile
     //Vector2f m_sizeOfTile;
     
    
    //Color of rectangle to start: white
     Color m_Color;
     

     bool black;
     bool white;

public:

	Tile();

	// Where is the center of the Tile
	Vector2f getCenter();
	Transformable setPosition(float x, float y);
	// void the functions
	void flippedBlack();
	void flippedWhite();
	RectangleShape getRectangle();
	RectangleShape getGlobalBounds();
    FloatRect getPosition();
	void setFillColor(Color color);
	void update();


};



#pragma once
