#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class Player
{
private:

	// Where is the player
	Vector2f m_Position;

	// Of course we will need a sprite
	Sprite m_Sprite;

	// And a texture
	// !!Watch this space!!
	Texture m_Texture;
	
	//enum PlayerDir{Left, Right, Down, Up};
	
	//const static int doctorWidth = 40;
	//const static int doctorHeight = 54;
	//sf::Vector2i select;


	// Which directions is the player currently moving in
	bool m_UpPressed;
	bool m_DownPressed;
	bool m_LeftPressed;
	bool m_RightPressed;

	// All our public functions will come next
public:   

	Player();

	// Where is the center of the player
	Vector2f getCenter();


	// Send a copy of the sprite to main
	Sprite getSprite();
	Sprite getGlobalBounds();
     FloatRect getPosition();

	void spawn();
	// The next four functions move the player
	void moveLeft();

	void moveRight();

	void moveUp();

	void moveDown();

	// Stop the player moving in a specific direction
	void stopLeft();

	void stopRight();

	void stopUp();

	void stopDown();

	// We will call this function once every frame
	void update();



};



#pragma once
