#include "Player.h"
#include "TextureHolder.h"

Player::Player()
{

	// Associate a texture with the sprite
	// !!Watch this space!!
	m_Sprite = Sprite(TextureHolder::GetTexture("images/scientist2.png"));

	// Set the origin of the sprite to the centre, 
	// for smooth rotation
	m_Sprite.setOrigin(25, 25);
	m_Position.x=110;
	m_Position.y=290;
	
	//select.x = 1;
	//select.y = Right;
	
}





Vector2f Player::getCenter()
{
	return m_Position;
}

Sprite Player::getSprite()
{
	return m_Sprite;
}

FloatRect Player::getPosition(){
 return m_Sprite.getGlobalBounds();
}

void Player::moveLeft()
{
	m_LeftPressed = true;
	/*select.y = Left;
	++select.x;
	if(select.x * doctorWidth >= m_Texture.getSize().x)
	{
		select.x = 0;
	}*/
	
}

void Player::moveRight()
{
	m_RightPressed = true;
}

void Player::moveUp()
{
	m_UpPressed = true;
}

void Player::moveDown()
{
	m_DownPressed = true;
}

void Player::stopLeft()
{
	m_LeftPressed = false;
}

void Player::stopRight()
{
	m_RightPressed = false;
}

void Player::stopUp()
{
	m_UpPressed = false;
}

void Player::stopDown()
{
	m_DownPressed = false;
}




void Player::update()
{

	if (m_UpPressed)
	{
		m_Position.y -= 5;
	}

	if (m_DownPressed)
	{
		m_Position.y += 5;
	}

	if (m_RightPressed)
	{
		m_Position.x += 3;
	}

	if (m_LeftPressed)
	{	
	//m_Sprite.setTextureRect(sf::IntRect(select.x * doctorWidth, select.y * doctorHeight, doctorWidth, doctorHeight));
		m_Position.x -= 3;
	}

	m_Sprite.setPosition(m_Position);

// Keep the player in the arena
	if (m_Position.x >  790)
	{

    std::cout << "You're too far right" << std::endl;
		m_Position.x = 790;
	}

	if (m_Position.x < 20)
	{
    std::cout << "You're too far left" << std::endl;
		m_Position.x = 20;
	}

	if (m_Position.y > 580)
	{
    std::cout << "You're too low" << std::endl;
		m_Position.y = 580;
		//m_Position.y = m_Arena.height - m_TileSize;
	}

	if (m_Position.y < 20)
	{
    std::cout << "You're too high" << std::endl;
		m_Position.y = 20;
		//m_Position.y = m_Arena.top + m_TileSize;
	}



}



