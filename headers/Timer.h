#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class Timer
{
private:


	// Of course we will need a sprite
	Time m_Time;
	Text m_Text;
	Clock m_Clock;


	// the time the player starts out with
	int totalTime= seconds(30);

	// All our public functions will come next
public:

	Timer();

	// Where is the center of the player
	Vector2f getCenter();


	// Send a copy of the sprite to main
	Time getTime();


	// We will call this function once every frame
	void update();



};



#pragma once
