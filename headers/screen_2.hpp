#include <iostream> 
#include <sstream> 
#include <cstdlib> 
#include "Player.cpp"
#include "TextureHolder.cpp"
#include "Player.h"
#include "TextureHolder.h"
#include <SFML/Graphics.hpp> 
#include <SFML/Audio.hpp>

  class screen_2: public cScreen {
    private: float movement_step;
    float posx;
    float posy;
    sf::RectangleShape Rectangle;
    public: screen_2(void);
    virtual int Run(sf::RenderWindow & App);
  };

screen_2::screen_2(void) {
  movement_step = 5;
  posx = 320;
  posy = 240;
  //Setting sprite
  Rectangle.setFillColor(sf::Color(255, 255, 255, 150));
  Rectangle.setSize({10.f,10.f});
}

int screen_2::Run(sf::RenderWindow & App) {
	App.clear(sf::Color::White);
	sf::Event Event;
	sf::Font font;
	sf::Sprite gamebackground;
	sf::Texture backgroundImage;
	TextureHolder holder;
	Player player;
	sf::Sprite gametoken;
	sf::Vector2i screenDimensions(800,600);
	sf::Sound sound;
	sf::SoundBuffer soundBuffer;
	sf::Music music;
	sf::Clock frameClock;  
	sf::Text text;
	text.setCharacterSize(15);  
	text.setString("press tab to pause the game");
	text.setFont(font);
	text.setPosition(480,580);
	sf::Texture gametokentexture;
     text.setColor(sf::Color::Black);
	sf::SoundBuffer walkingnoise;
	sf::SoundBuffer flippingnoise;
	sf::Sound walknoise;
	sf::Sound flipnoise;
	
if(!flippingnoise.loadFromFile("../audio/click_and_slide.wav")){
	std::cout<<"Failed to load flipping sound"<< std::endl;
	return -1;
}
if(!walkingnoise.loadFromFile("../audio/one_hit_from_chinese_blocks.wav")){
	std::cout<<"Failed to load player walking noise" << std::endl;
return -1;
}
walknoise.setBuffer(walkingnoise);
flipnoise.setBuffer(flippingnoise);
   if(!gametokentexture.loadFromFile("../images/gold_coin.gif"))
     {
	std::cout<<"Failed to load player spritesheet" << std::endl;
	return 1;
      }

	sf::Texture texture;
    if (!texture.loadFromFile("../images/scientist2.png"))
    {
        std::cout << "Failed to load player spritesheet!" << std::endl;
        return 1;
    }
    float speed = 80.f;
    bool noKeyWasPressed = true;

if (!music.openFromFile("../audio/bgmusic.wav"))
    return -1; // error
gametoken.setPosition(300,200);
  music.play();
music.setLoop(true);
  Texture textureBackground = TextureHolder::GetTexture(
    "../images/gamebackground2.png");

  if (!backgroundImage.loadFromFile("../images/gamebackground2.png"))
    return 1;

  if (!font.loadFromFile("../fonts/KGLegoHouse.ttf"))
    return 1;

  gamebackground.setTexture(backgroundImage);
  gametoken.setTexture(gametokentexture);
  bool Running = true;


  while (Running) {
    //Verifying events
    while (App.pollEvent(Event)) {


        sf::Time frameTime = frameClock.restart();
        sf::Vector2f movement(0.f, 0.f);

      switch (Event.key.code) {
      case sf::Keyboard::Escape:
        return (0);
        break;
      default:
        break;
      }
      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
        std::cout << "Trying to go back to the main menu" << std::endl;
      }
      // Window closed
      if (Event.type == sf::Event::Closed) {
        return (-1);
      }
      if (Keyboard::isKeyPressed(Keyboard::W)) {
		player.moveUp();  
		walknoise.play();
		std::cout<< walknoise.getStatus() << std::endl;
		noKeyWasPressed = false;
      } else {
        player.stopUp();
      }

      if (Keyboard::isKeyPressed(Keyboard::S)) {
        player.moveDown();
		walknoise.play();
		noKeyWasPressed = false;
      } else {
        player.stopDown();
      }

      if (Keyboard::isKeyPressed(Keyboard::A)) {
        player.moveLeft();
		walknoise.play();
		noKeyWasPressed = false;
      } else {
		walknoise.play();
        player.stopLeft();
      }

      if (Keyboard::isKeyPressed(Keyboard::D)) {
        player.moveRight();
		walknoise.play();
		noKeyWasPressed = false;
      } else {
        player.stopRight();
      }
      if(Keyboard::isKeyPressed(Keyboard::Space)){
		//to have tile flip action soon
		flipnoise.play();	
	}

      //Update Player
      player.update();
      App.clear();

    }

    //Updating
	App.draw(gamebackground);
	App.draw(player.getSprite());
	App.draw(gametoken);
	App.draw(text);
	//Clearing screen
	//Drawing
	App.display();
  }

  //Never reaching this point normally, but just in case, exit the application
  return -1;
}
