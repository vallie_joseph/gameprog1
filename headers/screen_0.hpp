#include <iostream>

#include <SFML/Graphics.hpp>

class screen_0 : public cScreen
{
private:
    int alpha_max;
    int alpha_div;
    bool playing;
public:
    screen_0(void);
    virtual int Run(sf::RenderWindow &App);
};

screen_0::screen_0(void)
{
    alpha_max = 3 * 255;
    alpha_div = 3;
    playing = false;
}

int screen_0::Run(sf::RenderWindow &App)
{
	sf::Event Event;
	bool Running = true;
	sf::Texture Texture;
	sf::Texture titleAvatarImage;
	sf::Sprite Sprite;
	sf::Sprite nicole;
	int alpha = 0;
	sf::Font Font;
	sf::Text Menu1;
	sf::Text Menu2;
	sf::Text Menu3;
	sf::Text Options;
	sf::Text Exit;
	sf::Text escapeHelp;
	Sprite.setPosition(150,150);
	nicole.setPosition(-10,350);
	sf::RectangleShape play;
	sf::RectangleShape instructions;
	sf::RectangleShape options;
	sf::RectangleShape exit;

	exit.setSize(sf::Vector2f(150, 50));
	exit.setFillColor(sf::Color::Red);
	exit.setPosition(340,600);

     options.setSize(sf::Vector2f(150, 50));
	options.setFillColor(sf::Color::Red);
	options.setPosition(370,400);

	play.setSize(sf::Vector2f(150, 50));
	play.setFillColor(sf::Color::Red);
	play.setPosition(200,400);

	instructions.setSize(sf::Vector2f(150, 50));
	instructions.setFillColor(sf::Color::Red);
	instructions.setPosition(540,400);
    int menu = 0;

    if (!Texture.loadFromFile("../images/flip-transparent.png"))
    {
        std::cerr << "Error loading transparent.gif" << std::endl;
        return (-1);
    }
if (!titleAvatarImage.loadFromFile("../images/nicole-title.png"))
    {
        std::cerr << "Error loading transparent.gif" << std::endl;
        return (-1);
    }
    Sprite.setTexture(Texture);
	nicole.setTexture(titleAvatarImage);
    Sprite.setColor(sf::Color(255, 255, 255, alpha));
    if (!Font.loadFromFile("../fonts/KGLegoHouse.ttf"))
    {
        std::cerr << "Error loading KGLegoHouse.ttf" << std::endl;
        return (-1);
    }


	Options.setFont(Font);
	Options.setCharacterSize(20);
	Options.setString("Options");
	Options.setPosition(380,410);

	Exit.setFont(Font);
	escapeHelp.setFont(Font);
	escapeHelp.setCharacterSize(20);
	escapeHelp.setString("hi");
	escapeHelp.setPosition(480,500);

	Menu1.setFont(Font);
	Menu1.setCharacterSize(20);
	Menu1.setString("Play");
	Menu1.setPosition(250,410);

	Menu2.setFont(Font);
	Menu2.setCharacterSize(20);
	Menu2.setString("Credits");
	Menu2.setPosition(540,410);

	Menu3.setFont(Font);
	Menu3.setCharacterSize(20);
	Menu3.setString("Continue");
	Menu3.setPosition(250,410);

    if (playing)
    {
        alpha = alpha_max;
    }

    while (Running)
    {
        //Verifying events
        while (App.pollEvent(Event))
        {
if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
      {
         App.close();
      }
            // Window closed
            if (Event.type == sf::Event::Closed)
            {
                return (-1);
            }
            //Key pressed
            if (Event.type == sf::Event::KeyPressed)
            {
                switch (Event.key.code)
                {
                case sf::Keyboard::Right:
                    menu = 0;
                    break;
                case sf::Keyboard::Left:
                    menu = 2;
                    break;
                case sf::Keyboard::Return:
                    if (menu == 0)
                    {
                        //Let's get play !
                        return (1);
                    }
                    else if(menu == 2)
                    {
						playing = true;
						return (2);
					}
					else
                    {
                        //Let's get work...
                        //return (-1);
                    }
                    break;
                default:
                    break;
                }
            }
        }
        //When getting at alpha_max, we stop modifying the sprite
        if (alpha<alpha_max)
        {
            alpha++;
        }
        Sprite.setColor(sf::Color(255, 255, 255, alpha / alpha_div));
        if (menu == 0)
        {
            Menu1.setColor(sf::Color::Black);
            Menu2.setColor(sf::Color::White);
            Menu3.setColor(sf::Color::Black);
        }
        else
        {
            Menu1.setColor(sf::Color::White);
            Menu2.setColor(sf::Color::Black);
            Menu3.setColor(sf::Color::White);
        }

        //Clearing screen
        App.clear(sf::Color::White);
        //Drawing
		App.draw(nicole);
		App.draw(Sprite);
		App.draw(play);
		App.draw(exit);
		App.draw(options);
		App.draw(Options);
		App.draw(instructions);
		App.draw(escapeHelp);

        if (alpha == alpha_max)
        {
            if (playing)
            {
                App.draw(Menu3);
            }
            else
            {
                App.draw(Menu1);
            }
            App.draw(Menu2);
        }
        App.display();
    }

    //Never reaching this point normally, but just in case, exit the application
    return (-1);
}
