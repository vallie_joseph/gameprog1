#include <iostream>

#include <SFML/Graphics.hpp>
class screen_1 : public cScreen
{
private:
    float movement_step;
    float posx;
    float posy;
    sf::RectangleShape Rectangle;
public:
    screen_1(void);
    virtual int Run(sf::RenderWindow &App);
};

screen_1::screen_1(void)
{
    movement_step = 5;
    posx = 320;
    posy = 240;
    //Setting sprite
    Rectangle.setFillColor(sf::Color(255, 255, 255, 150));
    Rectangle.setSize({ 10.f, 10.f });
}

int screen_1::Run(sf::RenderWindow &App)
{   
  App.clear(sf::Color::White);
	 sf::Font font;
	 sf::Text musicText;
	 sf::Text soundFxText; 
	 sf::Text signage;
	 sf::Text goBack;

	 if (!font.loadFromFile("../fonts/KGLegoHouse.ttf"))
		return 1;
	 soundFxText.setFont(font); 
	 signage.setFont(font); 
	 musicText.setFont(font); 
	 goBack.setFont(font); 
    sf::Event Event;
    bool Running = true;

    while (Running)
    {
        //Verifying events
        while (App.pollEvent(Event))
        {
switch (Event.key.code)
                {
                case sf::Keyboard::Escape:
                    return (0);
                    break;
			 default:
                    break;
                }
if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
      {
        std::cout<<"Trying to go back to the main menu"<< std::endl;
      }
            // Window closed
            if (Event.type == sf::Event::Closed)
            {
                return (-1);
            }
            //Key pressed
   
      
	musicText.setString("Music \n o Techno/ house beat type background music \n http://www.freesound.org/people/Creeper_Ciller78/sounds/346895/ \n o Menu Music/Pause music \n http://www.freesound.org/people/Creeper_Ciller78/sounds/346895/ (softer) \n o Final Battle Music\n  http://incompetech.com/music/royalty-free/index.html?collection=12&Search=Search (Exit the Premises)");
 	
 	soundFxText.setString("Sound FX \n o Tile Flip Sound Effect \n http://www.zapsplat.com/music/cartoon-swipe-grab-or-transition-med-pitched/ \n o Building Level Sound \n http://www.zapsplat.com/music/fantasy-reversed-debris-building-or-structure-re-assemble-by-magic/  ");
	 
 	signage.setString("Created by: Vallie Joseph \n Location: Marist College \n Project Title: C++ Game Project for CMPT 414N 111 16F (Game Programming and Design I) ");
	goBack.setString("Press ESC to go back to the main menu");

 	musicText.setPosition(40,50);
	 musicText.setCharacterSize(14);
	 musicText.setFont(font); 
	 musicText.setColor(sf::Color::Black);

	 soundFxText.setPosition(40,255);
	 soundFxText.setCharacterSize(14);
	 soundFxText.setFont(font); 
	 soundFxText.setColor(sf::Color::Black);

 	 signage.setPosition(40,410);
	 signage.setCharacterSize(14);
	 signage.setFont(font); 
	 signage.setColor(sf::Color::Black);

      goBack.setPosition(480,0);
	 goBack.setCharacterSize(14);
	 goBack.setFont(font); 
	 goBack.setColor(sf::Color::Red);


        }

        //Updating
	   App.draw(musicText);
	   App.draw(soundFxText);
	   App.draw(signage);
	   App.draw(goBack);
        //Clearing screen
        //Drawing
        App.draw(Rectangle);
        App.display();
    }

    //Never reaching this point normally, but just in case, exit the application
    return -1;
}
