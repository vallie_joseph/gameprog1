#include "Player.h"
#include "TextureHolder.h"

Player::Player()
{

	// Associate a texture with the sprite
	// !!Watch this space!!
	m_Sprite = Sprite(TextureHolder::GetTexture(
		"../images/scientist2.png"));

	// Set the origin of the sprite to the centre, 
	// for smooth rotation
	m_Sprite.setOrigin(25, 25);

	m_Position.x=110;
	m_Position.y=290;
	
}





Vector2f Player::getCenter()
{
	return m_Position;
}

Sprite Player::getSprite()
{
	return m_Sprite;
}


void Player::moveLeft()
{
	m_LeftPressed = true;
}

void Player::moveRight()
{
	m_RightPressed = true;
}

void Player::moveUp()
{
	m_UpPressed = true;
}

void Player::moveDown()
{
	m_DownPressed = true;
}

void Player::stopLeft()
{
	m_LeftPressed = false;
}

void Player::stopRight()
{
	m_RightPressed = false;
}

void Player::stopUp()
{
	m_UpPressed = false;
}

void Player::stopDown()
{
	m_DownPressed = false;
}

/*void Player::spawn(Vector2f resolution)
{
	// Place the player in the middle of the arena
	m_Position.x = 800 / 2;
	m_Position.y = 600 / 2;

}*/


void Player::update()
{

	if (m_UpPressed)
	{
		m_Position.y -= 45;
	}

	if (m_DownPressed)
	{
		m_Position.y += 45;
	}

	if (m_RightPressed)
	{
		m_Position.x += 35;
	}

	if (m_LeftPressed)
	{
		m_Position.x -= 35;
	}

	m_Sprite.setPosition(m_Position);

// Keep the player in the arena
	if (m_Position.x >  790)
	{

    std::cout << "Your too far right" << std::endl;
		m_Position.x = 790;
	}

	if (m_Position.x < 20)
	{
    std::cout << "Your too far left" << std::endl;
		m_Position.x = 20;
	}

	if (m_Position.y > 580)
	{
    std::cout << "You're too low" << std::endl;
		m_Position.y = 580;
		//m_Position.y = m_Arena.height - m_TileSize;
	}

	if (m_Position.y < 20)
	{
    std::cout << "You're too high" << std::endl;
		m_Position.y = 20;
		//m_Position.y = m_Arena.top + m_TileSize;
	}


}



