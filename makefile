CXX = g++
CXXFLAGS = -std=c++11 -pedantic
CXXLIBS = -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system 
DEBUGFLAGS = -g -Wall

all: Flip 

Flip: code/main-flip.o
	$(CXX) $< -o $@ $(CXXLIBS)

code/%.o: %.cpp
	$(CXX) $< -c -o $@

clean:
	rm Flip code/*.o 
